import 'package:flutter/material.dart';

class PageD extends StatefulWidget {
  PageD({Key key}) : super(key: key);

  @override
  _PageDState createState() => _PageDState();
}

class _PageDState extends State<PageD> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [Text('page d')],
      ),
    );
  }
}
